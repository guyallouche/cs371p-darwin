// ----------------
// RunDarwin.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <cstdio>
#include <vector>
#include <sstream>
#include <string>
#include <cstdlib>

using std::getline;
using std::string;
using std::stoi;
using std::istream;
using std::ostream;

#include "Darwin.hpp"

void print_board(ostream&, const Darwin& darwin);

void darwin_IO (istream& sin, ostream& sout) {
    //set up species
    Species food("f");
    food.addInstruction(left);
    food.addInstruction(go, 0);
    Species hopper("h");
    hopper.addInstruction(hop);
    hopper.addInstruction(go, 0);
    Species rover("r");
    rover.addInstruction(if_enemy, 9);
    rover.addInstruction(if_empty, 7);
    rover.addInstruction(if_random, 5);
    rover.addInstruction(left);
    rover.addInstruction(go, 0);
    rover.addInstruction(right);
    rover.addInstruction(go, 0);
    rover.addInstruction(hop);
    rover.addInstruction(go, 0);
    rover.addInstruction(infect);
    rover.addInstruction(go, 0);
    Species trap("t");
    trap.addInstruction(if_enemy, 3);
    trap.addInstruction(left);
    trap.addInstruction(go, 0);
    trap.addInstruction(infect);
    trap.addInstruction(go, 0);

    using namespace std;

    string s;
    // first line is number of test cases
    getline(sin, s);
    int t = stoi(s);
    assert(t > 0);
    for (int testcase = 0; testcase < t; testcase++) {
        srand(0);
        getline(sin, s); //there is a blank line
        int rows;
        int cols;
        getline(sin, s);
        std::stringstream ss (s);
        ss >> rows;
        ss >> cols;
        getline(sin, s);
        int numCreatures = stoi(s);
        Darwin darwin(rows, cols, numCreatures);
        for (int i = 0; i < numCreatures; i++) {
            getline(sin, s);
            std::stringstream ss (s);
            string c;
            int row;
            int col;
            string dir;
            ss >> c;
            ss >> row;
            ss >> col;
            ss >> dir;

            Species* species;
            if (c == "f") {
                species = &food;
            } else if (c == "h") {
                species = &hopper;
            } else if (c == "r") {
                species = &rover;
            } else {
                species = &trap;
            }
            dir_t direction;
            if (dir == "e") {
                direction = east;
            } else if (dir == "n") {
                direction = north;
            } else if (dir == "w") {
                direction = west;
            } else {
                direction = south;
            }
            Creature creature(*species, direction, c);
            darwin.addCreature(creature, row, col);
        }
        getline(sin, s);
        ss = stringstream(s);
        int turns;
        int freq;
        ss >> turns;
        ss >> freq;
        sout << "*** Darwin " << rows << "x" << cols << " ***" << "\n";
        for (int turn = 0; turn <= turns; turn++) {
            if (turn % freq == 0) {
                sout << "Turn = " << turn << "." << "\n";
                print_board(sout, darwin);
                if (turns/freq != turn/freq) {
                    sout << "\n";
                }
            }
            darwin.advance();
        }
        if (testcase != t-1) {
            sout << "\n";
        }

    }
}

using namespace std;

void print_board(ostream& sout, const Darwin& darwin) {
    int rows = darwin.board.size();
    int cols = darwin.board[0].size();
    sout << "  ";
    for (int i = 0; i < cols; i++) {
        sout << i%10;
    }
    sout << "\n";
    for (int i = 0; i < rows; i++) {
        sout << i%10 << " ";
        for (int j = 0; j < cols; j++) {
            if (darwin.board[i][j] != NULL) {
                sout << darwin.board[i][j]->symbol;
            } else {
                sout << ".";
            }
        }
        sout << "\n";
    }
}


int main () {
    darwin_IO(cin, cout);
    return 0;
}
