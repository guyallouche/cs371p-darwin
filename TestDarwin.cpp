// ---------------
// TestDarwin.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <vector>

#include "gtest/gtest.h"

#include "Darwin.hpp"


// --------
// contains
// --------

TEST(DarwinFixture, constructor)
{
    Darwin test(10, 8);
    ASSERT_EQ(test.board.size(), 10);
    ASSERT_EQ(test.board[0].size(), 8);
}

TEST(DarwinFixture, addCreature) {
    Darwin test(10, 8);
    Species s;
    Creature c(s, 0, north);
    test.addCreature(c, 3, 4);
    ASSERT_EQ(!test.board[3][3], true);
    ASSERT_EQ(!test.board[3][4], false);
}

TEST(DarwinFixture, addCreature2) {
    Darwin test(10, 8);
    Species s;
    Creature c(s, north);
    test.addCreature(c, 3, 4);
    test.addCreature(c, 3, 3);
    ASSERT_EQ(!test.board[3][3], false);
    ASSERT_EQ(!test.board[3][4], false);

}

TEST(DarwinFixture, addCreature3) {
    Darwin test(10, 8);
    Species s;
    Creature c(s, north);
    Creature c2(s, north);
    test.addCreature(c, 3, 4);
    test.addCreature(c, 3, 4);
    ASSERT_EQ(!test.board[3][3], true);
    ASSERT_EQ(!test.board[3][4], false);

}

TEST(DarwinFixture, facing1) {
    Darwin test(10, 8);
    Species s;
    Creature c(s, 0, north);
    test.addCreature(c, 3, 4);
    ASSERT_EQ(c.facing(test.board, 3, 4), emptySpace);
}

TEST(DarwinFixture, facing2) {
    Darwin test(10, 8);
    Species s;
    Creature c(s, 0, north);
    test.addCreature(c, 0, 4);
    ASSERT_EQ(c.facing(test.board, 0, 4), wall);
}

TEST(DarwinFixture, facing3) {
    Darwin test(10, 8);
    Species s;
    Creature c1(s, 0, north);
    test.addCreature(c1, 3, 4);
    Creature c2(s, 0, north);
    test.addCreature(c2, 2, 4);
    ASSERT_EQ(c1.facing(test.board, 3, 4), sameSpecies);
}

TEST(DarwinFixture, facing4) {
    Darwin test(10, 8);
    Species s1;
    Species s2;
    Creature c1(s1, 0, north);
    test.addCreature(c1, 3, 4);
    Creature c2(s2, 0, north);
    test.addCreature(c2, 2, 4);
    ASSERT_EQ(c1.facing(test.board, 3, 4), differentSpecies);
}

TEST(DarwinFixture, addInstructionPlusExecute) {
    Species s1;
    s1.addInstruction(hop);
    s1.addInstruction(go, 2);
    s1.addInstruction(infect);
    s1.addInstruction(if_empty, 0);
    s1.addInstruction(go, 2);
    face_t facing = emptySpace;
    Signal sig = s1.execute(0, facing);
    ASSERT_EQ(sig.instruction, hop);
    ASSERT_EQ(sig.newProgramCounter, 1);
    sig = s1.execute(sig.newProgramCounter, facing);
    ASSERT_EQ(sig.instruction, infect);
    ASSERT_EQ(sig.newProgramCounter, 3);
    ASSERT_EQ(s1.execute(sig.newProgramCounter, facing).instruction, hop);
}


TEST(DarwinFixture, act1) {
    Darwin test(10, 8);
    Species s1;
    s1.addInstruction(left);
    s1.addInstruction(right);
    s1.addInstruction(right);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    Species s2;
    Creature c2(s2, west);
    test.addCreature(c1, 0, 4);
    test.addCreature(c2, 0, 3);
    ASSERT_EQ(c1.facing(test.board, 0, 4), wall);
    ASSERT_EQ(c1.act(c1.facing(test.board, 0, 4), 0, 4, 0).instruction, left);
    ASSERT_EQ(c1.facing(test.board, 0, 4), differentSpecies);
    ASSERT_EQ(c1.act(c1.facing(test.board, 0, 4), 0, 4, 1).instruction, right);
    ASSERT_EQ(c1.facing(test.board, 0, 4), wall);
    ASSERT_EQ(c1.act(c1.facing(test.board, 0, 4), 0,4, 2).instruction, right);
    ASSERT_EQ(c1.facing(test.board, 0, 4), emptySpace);
}


TEST(DarwinFixture, execute1) {
    srand(0);
    Species s1;
    s1.addInstruction(if_random, 3);
    s1.addInstruction(left);
    s1.addInstruction(go, 0);
    s1.addInstruction(right);
    s1.addInstruction(go, 0);
    face_t facing = emptySpace;
    int programCounter = 0;
    ASSERT_EQ(s1.execute(programCounter, facing).instruction, right);
    ASSERT_EQ(s1.execute(programCounter, facing).instruction, left);
    ASSERT_EQ(s1.execute(programCounter, facing).instruction, right);
    ASSERT_EQ(s1.execute(programCounter, facing).instruction, right);
}


TEST(DarwinFixture, execute2) {
    srand(0);
    Species s1;
    s1.addInstruction(if_enemy, 3);
    s1.addInstruction(left);
    s1.addInstruction(go, 0);
    s1.addInstruction(infect);
    s1.addInstruction(go, 0);
    Signal sig = s1.execute(0, differentSpecies);
    ASSERT_EQ(sig.instruction, infect);
    ASSERT_EQ(sig.newProgramCounter, 4);
    ASSERT_EQ(s1.execute(sig.newProgramCounter, sameSpecies).instruction, left);
}

TEST(DarwinFixture, advancePlusHop) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    test.addCreature(c1, 3, 4);
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], true);
    test.advance();
    ASSERT_EQ(!test.board[3][4], true);
    ASSERT_EQ(!test.board[2][4], false);
    test.advance();
    ASSERT_EQ(!test.board[2][4], true);
    ASSERT_EQ(!test.board[1][4], false);
    test.advance();
    ASSERT_EQ(!test.board[1][4], true);
    ASSERT_EQ(!test.board[0][4], false);
    test.advance();
    ASSERT_EQ(!test.board[0][4], false);
}

TEST(DarwinFixture, hop2) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    Creature c2(s1, south);
    test.addCreature(c1, 3, 4);
    test.addCreature(c2, 2, 4);
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
    test.advance();
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
    test.advance();
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
}

TEST(DarwinFixture, hop3) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    test.addCreature(c1, 0, 4);
    ASSERT_EQ(!test.board[0][4], false);
    test.advance();
    ASSERT_EQ(!test.board[0][4], false);
    test.advance();
    ASSERT_EQ(!test.board[0][4], false);

}

TEST(DarwinFixture, sameSpecies) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(left);
    s1.addInstruction(go, 0);
    Species s2;
    s2.addInstruction(right);
    s2.addInstruction(go, 0);
    Creature c1(s1, north);
    Creature c2(s1, west);
    Creature c3(s2, north);
    test.addCreature(c1, 3, 4);
    test.addCreature(c2, 2, 4);
    test.addCreature(c3, 3, 3);
    ASSERT_EQ(test.board[3][4]->facing(test.board, 3, 4), sameSpecies);
    test.advance();
    ASSERT_EQ(test.board[3][4]->facing(test.board, 3, 4), differentSpecies);
    test.advance();
    ASSERT_EQ(test.board[3][4]->facing(test.board, 3, 4), emptySpace);
}

TEST(DarwinFixture, infect) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(infect);
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    test.addCreature(c1, 3, 4);
    Species s2;
    s2.addInstruction(nop);
    s2.addInstruction(go, 0);
    Creature c2(s2, west);
    test.addCreature(c2, 2, 4);


    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
    ASSERT_EQ(test.board[3][4]->facing(test.board, 3, 4), differentSpecies);
    test.advance();
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
    ASSERT_EQ(test.board[3][4]->facing(test.board, 3, 4), sameSpecies);
    test.advance();
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
    test.advance();
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], true);
    ASSERT_EQ(!test.board[2][3], false);
}

TEST(DarwinFixture, infect2) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(infect);
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    test.addCreature(c1, 3, 4);
    Species s2;
    s2.addInstruction(nop);
    s2.addInstruction(go, 0);
    Creature c2(s2, west);
    test.addCreature(c2, 2, 4);


    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], false);
    ASSERT_EQ(test.board[3][4]->facing(test.board, 3, 4), differentSpecies);
}



TEST(DarwinFixture, infect3) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(if_wall, 6);
    s1.addInstruction(infect);
    s1.addInstruction(infect);
    s1.addInstruction(infect);
    s1.addInstruction(infect);
    s1.addInstruction(go, 0);
    s1.addInstruction(right);
    s1.addInstruction(hop);
    s1.addInstruction(hop);
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Creature c1(s1, north);
    test.addCreature(c1, 1, 1);
    Creature c2(s1, north);
    test.addCreature(c2, 0, 0);


    ASSERT_EQ(!test.board[0][0], false);
    ASSERT_EQ(!test.board[1][1], false);
    test.advance();
    ASSERT_EQ(!test.board[0][0], false);
    ASSERT_EQ(!test.board[1][1], false);
    test.advance();
    ASSERT_EQ(!test.board[0][1], false);
    ASSERT_EQ(!test.board[1][1], false);
    ASSERT_EQ(test.board[1][1]->facing(test.board, 1, 1), sameSpecies);
    test.advance();
    ASSERT_EQ(!test.board[0][2], false);
    ASSERT_EQ(!test.board[1][1], false);
}

TEST(DarwinFixture, overwrite) {
    Darwin test(8, 10);
    Species s1;
    s1.addInstruction(hop);
    s1.addInstruction(go, 0);
    Species s2;
    s2.addInstruction(left);
    s2.addInstruction(hop);
    s2.addInstruction(go, 0);
    Creature c1(s1, north);
    test.addCreature(c1, 3, 4);
    Creature c2(s2, north);
    test.addCreature(c2, 3, 4);


    ASSERT_EQ(!test.board[3][4], false);
    test.advance();
    ASSERT_EQ(!test.board[3][4], false);
    ASSERT_EQ(!test.board[2][4], true);
    test.advance();
    ASSERT_EQ(!test.board[3][4], true);
    ASSERT_EQ(!test.board[2][4], true);
    ASSERT_EQ(!test.board[3][3], false);

}



