// ----------------
// Creature.h
// ----------------

#ifndef Creature_h
#define Creature_h

// --------
// includes
// --------

#include <vector>
#include <string>
#include "Species.hpp"

using std::vector;
using std::string;

enum dir_t : int { east = 0, north = 1, west = 2, south = 3 };

//helper struct to relay both an instruction and supporting information to Darwin
struct Action {
    Action(instruction_t inst, Species* species) : instruction(inst), actorSpecies(species), targetRow(0), targetCol(0) {};
    Action(instruction_t inst, Species* species, int row, int col) : instruction(inst), actorSpecies(species), targetRow(row), targetCol(col) {};
    instruction_t instruction;
    Species* actorSpecies;
    int targetRow;
    int targetCol;
};


class Creature {
private:
    Species* species;
    int programCounter;
    dir_t direction;
    int lastActed = -1;


public:
    //public just for RunDarwin to print the board. never used in Darwin itself.
    string symbol;

    //two argument constructor - species + direction
    Creature (Species& s, dir_t dir);

    //three argument constructor - species + direction + program counter initialization
    Creature (Species& s, int PC, dir_t dir);

    //three argument constructor - species + direction + symbol
    Creature (Species& s, dir_t dir, string symbol);

    //takes in the board and returns only the contents of the square we're looking at
    face_t facing (const vector<vector<Creature*>>& board, int row, int col);

    //takes in the contents of the square we're facing and our location + the current turn, and executes whatever our
    //instructions say; returns the instruction + relevant info to Darwin
    Action act (face_t facing, int row, int col, int turn);

    //returns whether we're the same species as otherSpecies
    bool isSameSpecies (Species* otherSpecies);

    //converts this creature to newSpecies and resets program counter. Should only be called on other species.
    void convertSelf(Species* newSpecies);

};

#endif //Creature_h