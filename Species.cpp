// ----------------
// Species.c++
// ----------------

// --------
// includes
// --------

#include <vector>
#include <cassert>
#include "Species.hpp"
#include <cstdlib>
#include <cstdio>
#include <string>

using std::vector;
using std::string;

Species::Species() : ilist(), symbol() {

}

Species::Species(string symb) : ilist(), symbol(symb) {

}


void Species::addInstruction(instruction_t instruction) {
    struct Instruction inst(instruction, 0);
    ilist.push_back(inst);
}

void Species::addInstruction(instruction_t instruction, int targetLine) {
    struct Instruction inst(instruction, targetLine);
    ilist.push_back(inst);
}

Signal Species::execute(int programCounter, face_t facing) {
    assert(programCounter < ilist.size());
    assert(ilist.size() > 0);
    // essentially loop until we return. this can loop forever, but that's intentional (some instruction lists will loop forever)
    while (programCounter < ilist.size()) {
        struct Instruction inst = ilist[programCounter];
        if (inst.instruction <= lastAction) {
            programCounter++;
            return Signal(inst.instruction, programCounter);
        } else {
            switch(inst.instruction) {
            case if_empty:
                if (facing == emptySpace) {
                    programCounter = inst.targetLine;
                } else {
                    programCounter++;
                }
                break;
            case if_wall:
                if (facing == wall) {
                    programCounter = inst.targetLine;
                } else {
                    programCounter++;
                }
                break;
            case if_random:
                if (rand()%2==1) {
                    programCounter = inst.targetLine;
                } else {
                    programCounter++;
                }
                break;
            case if_enemy:
                if (facing == differentSpecies) {
                    programCounter = inst.targetLine;
                } else {
                    programCounter++;
                }
                break;
            case go:
                programCounter = inst.targetLine;
                break;
            }
        }
    }
    assert(false); //we should not be here!
}

