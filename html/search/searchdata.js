var indexSectionsWithContent =
{
  0: "abcdefghilmnprstw",
  1: "acdis",
  2: "cdhrst",
  3: "acdefimpst",
  4: "abcdilnpst",
  5: "dfi",
  6: "deghilnrsw",
  7: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Pages"
};

