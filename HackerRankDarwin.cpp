// ----------------
// RunDarwin.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <cstdio>
#include <vector>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cassert>

using std::getline;
using std::string;
using std::stoi;
using std::istream;
using std::ostream;
using std::vector;


enum face_t : int { emptySpace = 0, wall = 1, sameSpecies = 2, differentSpecies = 3 };
enum instruction_t : int {nop = -1, hop = 0, left = 1, right = 2, infect = 3, if_empty = 4, if_wall = 5, if_random = 6, if_enemy = 7, go = 8 };

const instruction_t lastAction = infect;

struct Instruction {
    Instruction(instruction_t inst, int target) : instruction(inst), targetLine(target) {};
    instruction_t instruction;
    int targetLine;
};

struct Signal {
    Signal(instruction_t inst, int pc) : instruction(inst), newProgramCounter(pc) {};
    instruction_t instruction;
    int newProgramCounter;
};

class Species {
    private:
        vector<Instruction> ilist;

    public: 
        string symbol;
        Species();
        Species(string symb);

        void addInstruction(instruction_t instruction);

        void addInstruction(instruction_t instruction, int targetLine);

        Signal execute(int programCounter, face_t facing);
        
};


Species::Species() : ilist(), symbol() {

}

Species::Species(string symb) : ilist(), symbol(symb) {

}


void Species::addInstruction(instruction_t instruction) {
    struct Instruction inst(instruction, 0);
    ilist.push_back(inst);
}

void Species::addInstruction(instruction_t instruction, int targetLine) {
    struct Instruction inst(instruction, targetLine);
    ilist.push_back(inst);
}

Signal Species::execute(int programCounter, face_t facing) {
    assert(programCounter < ilist.size());
    assert(ilist.size() > 0);
    // essentially loop until we return. this can loop forever, but that's intentional (some instruction lists will loop forever)
    while (programCounter < ilist.size()) {
        struct Instruction inst = ilist[programCounter]; 
        if (inst.instruction <= lastAction) {
            programCounter++;
            return Signal(inst.instruction, programCounter);
        } else {
            switch(inst.instruction) {
                case if_empty:
                    if (facing == emptySpace) {
                        programCounter = inst.targetLine;
                    } else {
                        programCounter++;
                    }
                    break;
                case if_wall:
                    if (facing == wall) {
                        programCounter = inst.targetLine;
                    } else {
                        programCounter++;
                    }
                    break;
                case if_random:
                    if (rand()%2==1) {
                        programCounter = inst.targetLine;
                    } else {
                        programCounter++;
                    }
                    break;
                case if_enemy:
                    if (facing == differentSpecies) {
                        programCounter = inst.targetLine;
                    } else {
                        programCounter++;
                    }
                    break;
                case go:
                    programCounter = inst.targetLine;
                    break;
            }
        }
    }
    assert(false); //we should not be here!
}



enum dir_t : int { east = 0, north = 1, west = 2, south = 3 };


struct Action {
    Action(instruction_t inst, Species* species) : instruction(inst), actorSpecies(species), targetRow(0), targetCol(0) {};
    Action(instruction_t inst, Species* species, int row, int col) : instruction(inst), actorSpecies(species), targetRow(row), targetCol(col) {};
    instruction_t instruction;
    Species* actorSpecies;
    int targetRow;
    int targetCol;
};


class Creature {
    private:
        Species* species;
        int programCounter;
        dir_t direction;
        int lastActed = -1;
        

    public:
        string symbol;

        Creature (Species& s, dir_t dir);

        Creature (Species& s, int PC, dir_t dir);

        Creature (Species& s, dir_t dir, string symbol);

        face_t facing (const vector<vector<Creature*>>& board, int row, int col);

        Action act (face_t facing, int row, int col, int turn);

        bool isSameSpecies (Species* otherSpecies);

        void convertSelf(Species* newSpecies);
        
};




Creature::Creature (Species& s, dir_t dir) : species(&s), programCounter(0), direction(dir), symbol() {

}

Creature::Creature (Species& s, int PC, dir_t dir) : species(&s), programCounter(PC), direction(dir), symbol() {
}

Creature::Creature (Species& s, dir_t dir, string string) : species(&s), programCounter(0), direction(dir), symbol(string) {
}

face_t Creature::facing (const vector<vector<Creature*>>& board, int row, int col) {
    switch (direction) {
        case east:
            if (col + 1 >= board[0].size()) {
                return wall;
            } else if (!board[row][col+1]) {
                return emptySpace;
            } else if (board[row][col+1]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;

        case north:
            if (row - 1 < 0) {
                return wall;
            } else if (!board[row-1][col]) {
                return emptySpace;
            } else if (board[row-1][col]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;

        case west:
            if (col - 1 < 0) {
                return wall;
            } else if (!board[row][col-1]) {
                return emptySpace;
            } else if (board[row][col-1]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;
        
        case south:
            if (row + 1 >= board.size()) {
                return wall;
            } else if (!board[row+1][col]) {
                return emptySpace;
            } else if (board[row+1][col]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;
    }
    assert(false); //should not get here
}

Action Creature::act (face_t facing, int row, int col, int turn) {
    if (lastActed >= turn) {
        //do nothing, we've already acted!
        struct Action ret(nop, NULL);
        return ret;
    }
    lastActed = turn;
    Signal sig = species->execute(programCounter, facing);
    programCounter = sig.newProgramCounter;
    instruction_t action = sig.instruction;
    assert(action <= lastAction);
    //if we gotta turn, turn!
    if (action == left) {
        direction = (dir_t) (((int)direction + 1)%4); 
    } else if (action == right) {
        direction = (dir_t) ((int)direction - 1);
        if (direction < east) {
            direction = south;
        }
    }
    int targetRow = row;
    int targetCol = col;
    if (direction == south) {
        targetRow++;
    } else if (direction == north) {
        targetRow--;
    } else if (direction == east) {
        targetCol++;
    } else if (direction == west) {
        targetCol--;
    }

    struct Action ret(action, species, targetRow, targetCol);
    return ret;
}

bool Creature::isSameSpecies(Species* otherSpecies) {
    return species == otherSpecies;
}

void Creature::convertSelf(Species* newSpecies) {
    assert(!isSameSpecies(newSpecies)); //should not be same species
    species = newSpecies;
    programCounter = 0;
    symbol = newSpecies->symbol;
}

class Darwin {

    public:
        vector<vector<Creature*>> board;
        vector<Creature> creatures;
        int turn = 0;

        Darwin(int rows, int cols);
        Darwin(int rows, int cols, int numCreatures);

        void addCreature(Creature creature, int row, int col);

        void advance();

};

Darwin::Darwin(int rows, int cols) : board(rows, vector<Creature*>(cols, NULL)) {
}

Darwin::Darwin(int rows, int cols, int numCreatures) : board(rows, vector<Creature*>(cols, NULL)) {
    creatures.reserve(numCreatures + 1);
}

void Darwin::addCreature(Creature creature, int row, int col) {
    creatures.push_back(creature);
    Creature* p = &creatures[creatures.size()-1];
    board[row][col] = p;
}

void Darwin::advance() {
    //iterate over all creatures and have them act on the board per their instructions.
    turn++;
    for (int row = 0; row < board.size(); row++) {
        for (int col = 0; col < board[0].size(); col++) {
            //if spot is occupied by a creature
            if (board[row][col]) {
                Creature& c = *board[row][col];
                face_t facing = c.facing(board, row, col);
                Action action = c.act(facing, row, col, turn);
                //darwin only has to do something here if the action was hop or infect
                assert(action.instruction <= lastAction);
                if (action.instruction == hop) {
                    //hop!
                    if (facing == emptySpace) {
                        board[row][col] = NULL;
                        board[action.targetRow][action.targetCol] = &c;
                    }
                } else if (action.instruction == infect) {
                    //infect. Tell infected creature to convert itself if it's a different species
                    if (facing == differentSpecies) {
                        board[action.targetRow][action.targetCol]->convertSelf(action.actorSpecies);
                    }
                }
            }
        }
    }
}




void print_board(ostream&, const Darwin& darwin);

void darwin_IO (istream& sin, ostream& sout) {
    //set up species
    Species food("f");
    food.addInstruction(left);
    food.addInstruction(go, 0);
    Species hopper("h");
    hopper.addInstruction(hop);
    hopper.addInstruction(go, 0);
    Species rover("r");
    rover.addInstruction(if_enemy, 9);
    rover.addInstruction(if_empty, 7);
    rover.addInstruction(if_random, 5);
    rover.addInstruction(left);
    rover.addInstruction(go, 0);
    rover.addInstruction(right);
    rover.addInstruction(go, 0);
    rover.addInstruction(hop);
    rover.addInstruction(go, 0);
    rover.addInstruction(infect);
    rover.addInstruction(go, 0);
    Species trap("t");
    trap.addInstruction(if_enemy, 3);
    trap.addInstruction(left);
    trap.addInstruction(go, 0);
    trap.addInstruction(infect);
    trap.addInstruction(go, 0);

    using namespace std;

    string s;
    // first line is number of test cases
    getline(sin, s);
    int t = stoi(s);
    assert(t > 0);
    for (int testcase = 0; testcase < t; testcase++) {
        srand(0);
        getline(sin, s); //there is a blank line
        int rows;
        int cols;
        getline(sin, s);
        std::stringstream ss (s);
        ss >> rows;
        ss >> cols;
        getline(sin, s);
        int numCreatures = stoi(s);
        Darwin darwin(rows, cols, numCreatures);
        for (int i = 0; i < numCreatures; i++) {
            getline(sin, s);
            std::stringstream ss (s);
            string c;
            int row;
            int col;
            string dir;
            ss >> c;
            ss >> row;
            ss >> col;
            ss >> dir;

            Species* species;
            if (c == "f") {
                species = &food;
            } else if (c == "h") {
                species = &hopper;
            } else if (c == "r") {
                species = &rover;
            } else {
                species = &trap;
            }
            dir_t direction;
            if (dir == "e") {
                direction = east;
            } else if (dir == "n") {
                direction = north;
            } else if (dir == "w") {
                direction = west;
            } else {
                direction = south;
            }
            Creature creature(*species, direction, c);
            darwin.addCreature(creature, row, col);
        }
        getline(sin, s);
        ss = stringstream(s);
        int turns;
        int freq;
        ss >> turns;
        ss >> freq;
        sout << "*** Darwin " << rows << "x" << cols << " ***" << "\n"; 
        for (int turn = 0; turn <= turns; turn++) {
            if (turn % freq == 0) {
                sout << "Turn = " << turn << "." << "\n";
                print_board(sout, darwin);
                if (turns/freq != turn/freq) {
                    sout << "\n";
                }
            }
            darwin.advance();
        }
        if (testcase != t-1) {
            sout << "\n";
        }

    }
}

using namespace std;

void print_board(ostream& sout, const Darwin& darwin) {
    int rows = darwin.board.size();
    int cols = darwin.board[0].size();
    sout << "  ";
    for (int i = 0; i < cols; i++) {
        sout << i%10;
    }
    sout << "\n";
    for (int i = 0; i < rows; i++) {
        sout << i%10 << " ";
        for (int j = 0; j < cols; j++) {
            if (darwin.board[i][j] != NULL) {
                sout << darwin.board[i][j]->symbol;
            } else {
                sout << ".";
            }
        }
        sout << "\n";
    }
}


int main () {
    darwin_IO(cin, cout);
    return 0;
}
