// -----------
// Darwin.h
// -----------

#ifndef Darwin_h
#define Darwin_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <vector>

#include "Creature.hpp"
#include "Species.hpp"

using std::vector;

// ---------
// Darwin
// ---------


class Darwin {

public:
    vector<vector<Creature*>> board;
    vector<Creature> creatures;
    int turn = 0;

    //two argument constructor - number of rows + number of columns. This constructor will not work for large test cases due to a bug.
    Darwin(int rows, int cols);

    //three argument constructor - num rows + num columns + number of creatures. This is actually the only correct one.
    Darwin(int rows, int cols, int numCreatures);

    //adds the specified creature to the board at specified row and col
    void addCreature(Creature creature, int row, int col);

    //advances the board one turn
    void advance();

};

#endif // Darwin_h
