# CS371p: Object-Oriented Programming Allocator Repo

* Name: Guy Allouche

* EID: ga22287

* GitLab ID: guyallouche

* HackerRank ID: guy_allouche_1

* Git SHA: 1ed0969c276c5305dbb7600bcaf55f3ff39bcc4a

* GitLab Pipelines: https://gitlab.com/guyallouche/cs371p-darwin/-/pipelines

* Estimated completion time: 6

* Actual completion time: 11

* Comments: My estimated time was a bit ambitious... Sorry it's late, this week has been hell. Things are going better now though :)
