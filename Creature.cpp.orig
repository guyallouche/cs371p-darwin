// ----------------
// Creature.h
// ----------------

// --------
// includes
// --------

#include <vector>

#include "Creature.hpp"
#include <cassert>
#include <cstdio>
#include <string>
#include <iostream>

using std::vector;

Creature::Creature (Species& s, dir_t dir) : species(&s), programCounter(0), direction(dir), symbol() {

}

Creature::Creature (Species& s, int PC, dir_t dir) : species(&s), programCounter(PC), direction(dir), symbol() {
}

Creature::Creature (Species& s, dir_t dir, string string) : species(&s), programCounter(0), direction(dir), symbol(string) {
}

face_t Creature::facing (const vector<vector<Creature*>>& board, int row, int col) {
    switch (direction) {
        case east:
            if (col + 1 >= board[0].size()) {
                return wall;
            } else if (!board[row][col+1]) {
                return emptySpace;
            } else if (board[row][col+1]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;

        case north:
            if (row - 1 < 0) {
                return wall;
            } else if (!board[row-1][col]) {
                return emptySpace;
            } else if (board[row-1][col]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;

        case west:
            if (col - 1 < 0) {
                return wall;
            } else if (!board[row][col-1]) {
                return emptySpace;
            } else if (board[row][col-1]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;
        
        case south:
            if (row + 1 >= board.size()) {
                return wall;
            } else if (!board[row+1][col]) {
                return emptySpace;
            } else if (board[row+1][col]->isSameSpecies(species)) {
                return sameSpecies;
            } else {
                return differentSpecies;
            }
            break;
    }
    assert(false); //should not get here
}

Action Creature::act (face_t facing, int row, int col, int turn) {
    if (lastActed >= turn) {
        //do nothing, we've already acted!
        struct Action ret(nop, NULL);
        return ret;
    }
    lastActed = turn;
    Signal sig = species->execute(programCounter, facing);
    programCounter = sig.newProgramCounter;
    instruction_t action = sig.instruction;
    assert(action <= lastAction);
    //if we gotta turn, turn!
    if (action == left) {
        direction = (dir_t) (((int)direction + 1)%4); 
    } else if (action == right) {
        direction = (dir_t) ((int)direction - 1);
        if (direction < east) {
            direction = south;
        }
    }
    int targetRow = row;
    int targetCol = col;
    if (direction == south) {
        targetRow++;
    } else if (direction == north) {
        targetRow--;
    } else if (direction == east) {
        targetCol++;
    } else if (direction == west) {
        targetCol--;
    }

    struct Action ret(action, species, targetRow, targetCol);
    return ret;
}

bool Creature::isSameSpecies(Species* otherSpecies) {
    return species == otherSpecies;
}

void Creature::convertSelf(Species* newSpecies) {
    assert(!isSameSpecies(newSpecies)); //should not be same species
    species = newSpecies;
    programCounter = 0;
    symbol = newSpecies->symbol;
}
