// ----------------
// Darwin.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <vector>

#include "Darwin.hpp"
#include "Creature.hpp"
#include "Species.hpp"


using std::vector;



Darwin::Darwin(int rows, int cols) : board(rows, vector<Creature*>(cols, NULL)) {
}

Darwin::Darwin(int rows, int cols, int numCreatures) : board(rows, vector<Creature*>(cols, NULL)) {
    creatures.reserve(numCreatures + 1);
}

void Darwin::addCreature(Creature creature, int row, int col) {
    creatures.push_back(creature);
    Creature* p = &creatures[creatures.size()-1];
    board[row][col] = p;
}

void Darwin::advance() {
    //iterate over all creatures and have them act on the board per their instructions.
    turn++;
    for (int row = 0; row < board.size(); row++) {
        for (int col = 0; col < board[0].size(); col++) {
            //if spot is occupied by a creature
            if (board[row][col]) {
                Creature& c = *board[row][col];
                face_t facing = c.facing(board, row, col);
                Action action = c.act(facing, row, col, turn);
                //darwin only has to do something here if the action was hop or infect
                assert(action.instruction <= lastAction);
                if (action.instruction == hop) {
                    //hop!
                    if (facing == emptySpace) {
                        board[row][col] = NULL;
                        board[action.targetRow][action.targetCol] = &c;
                    }
                } else if (action.instruction == infect) {
                    //infect. Tell infected creature to convert itself if it's a different species
                    if (facing == differentSpecies) {
                        board[action.targetRow][action.targetCol]->convertSelf(action.actorSpecies);
                    }
                }
            }
        }
    }
}


