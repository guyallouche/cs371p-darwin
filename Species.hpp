
#ifndef Species_h
#define Species_h

// --------
// includes
// --------

#include <vector>
#include <string>


using std::vector;
using std::string;

enum face_t : int { emptySpace = 0, wall = 1, sameSpecies = 2, differentSpecies = 3 };
enum instruction_t : int {nop = -1, hop = 0, left = 1, right = 2, infect = 3, if_empty = 4, if_wall = 5, if_random = 6, if_enemy = 7, go = 8 };

const instruction_t lastAction = infect;

//helper struct for an instruction + a target line (for control instructions)
struct Instruction {
    Instruction(instruction_t inst, int target) : instruction(inst), targetLine(target) {};
    instruction_t instruction;
    int targetLine;
};

//helper struct for an instruction + the new program counter to return to the Creature after we determine the action to take.
struct Signal {
    Signal(instruction_t inst, int pc) : instruction(inst), newProgramCounter(pc) {};
    instruction_t instruction;
    int newProgramCounter;
};

class Species {
private:
    vector<Instruction> ilist;

public:
    string symbol;
    //0 argument constructor, initializes ilist to empty and symbol is undefined
    Species();
    //1 argument constructor, initializes ilist to empty and symbol to argument
    Species(string symb);

    //add an instruction (no target line, must be an action)
    void addInstruction(instruction_t instruction);

    //add an instruction with target line; targetline is only meaningful for control instructions, but it works on action instructions too
    void addInstruction(instruction_t instruction, int targetLine);

    //executes instructions based on given program counter and whatever we're facing until we reach an action; return that and the
    //new program counter
    Signal execute(int programCounter, face_t facing);

};


#endif // Species_h